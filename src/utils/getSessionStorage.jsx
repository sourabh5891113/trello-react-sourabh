export default function getSessionStorage() {
    return JSON.parse(sessionStorage.getItem("rv")) || [];
}
