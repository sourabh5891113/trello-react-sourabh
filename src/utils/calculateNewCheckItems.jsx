export default function calculateNewCheckItems(checkItems,checkItemId) {
  const newCheckItems = checkItems.map((checkItem) => {
    if (checkItem.id === checkItemId) {
      let checkedState = checkItem.state;
      if (checkedState === "complete") {
        return { ...checkItem, state: "incomplete" };
      } else {
        return { ...checkItem, state: "complete" };
      }
    }
    return checkItem;
  });
  return newCheckItems;
}
