export default function setSessionStorage(id,board) {
  let rv = JSON.parse(sessionStorage.getItem("rv"));
  if (rv) {
    let isPresent = rv.find((board) => board.id === id);
    if (isPresent === undefined) {
      sessionStorage.setItem("rv", JSON.stringify([...rv, board]));
    }
  } else {
    sessionStorage.setItem("rv", JSON.stringify([board]));
  }
}
