export default function calculateProgress(checkItems) {
  const totalCheckItems = checkItems.length;
  if (totalCheckItems === 0) return 0;
  const checkedCheckItems = checkItems.reduce((acc, check) => {
    if (check.state === "complete") acc++;
    return acc;
  }, 0);
  const currentProgress = (checkedCheckItems / totalCheckItems) * 100;
  return currentProgress;
}
