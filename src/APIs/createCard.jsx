import axios from "axios";
import { apiKey, apiToken } from "../secretFile.jsx";

// export async function createCard(listId,cardName) {
//     let response = await fetch(`https://api.trello.com/1/cards?idList=${listId}&name=${cardName}&key=${apiKey}&token=${apiToken}`, {
//         method: 'POST',
//         headers: {
//           'Accept': 'application/json'
//         }
//       });
//       let data = response.json();
//       return data;
//   } 

  export async function createCard(cardName,listId) {
    try {
      const response = await axios.post(
        `https://api.trello.com/1/cards?idList=${listId}&name=${cardName}&key=${apiKey}&token=${apiToken}`
      );
      if (response.status === 200) {
        return response.data;
      }
    } catch (e) {
      console.error(e);
    }
  }