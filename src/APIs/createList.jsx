import axios from "axios";
import { apiKey, apiToken } from "../secretFile.jsx";

export async function createList(listName, boardId) {
  try {
    const response = await axios.post(
      `https://api.trello.com/1/lists?name=${listName}&idBoard=${boardId}&key=${apiKey}&token=${apiToken}`
    );
    if (response.status === 200) {
      return response.data;
    }
  } catch (e) {
    console.error(e);
  }
}
