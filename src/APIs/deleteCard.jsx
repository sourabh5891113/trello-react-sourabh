import axios from "axios";
import { apiKey, apiToken } from "../secretFile.jsx";

export async function deleteCard(cardId) {
  try {
    const response = await axios.delete(
      `https://api.trello.com/1/cards/${cardId}?key=${apiKey}&token=${apiToken}`
    );
    return response.data;
  } catch (error) {
    console.error(error);
  }
}
