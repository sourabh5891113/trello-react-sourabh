import {apiKey, apiToken} from "../secretFile.jsx"

export async function getMyBoards() {
  try {
    let response = await fetch(
      `https://api.trello.com/1/members/me/boards?fields=name,url,prefs&key=${apiKey}&token=${apiToken}`,
      {
        method: "GET",
        headers: {
          Accept: "application/json",
        },
      }
    );
    const data = await response.json();
    return data;
  } catch (error) {
    console.error("Error fetching data:", error);
  }
}
