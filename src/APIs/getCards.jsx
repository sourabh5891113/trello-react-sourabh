import axios from "axios";
import { apiKey, apiToken } from "../secretFile.jsx";

export async function getCards(listId) {
  try {
    const response = await axios.get(`https://api.trello.com/1/lists/${listId}/cards?fields=name,url&key=${apiKey}&token=${apiToken}`);
    return response.data;
  } catch (error) {
    console.error(error);
  }
}
