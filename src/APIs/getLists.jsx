import { apiKey, apiToken } from "../secretFile.jsx";
import axios from "axios";

export async function getLists(boardId) {
  try {
    const response = await axios.get(`https://api.trello.com/1/boards/${boardId}/lists?fields=name,idBoard&key=${apiKey}&token=${apiToken}`);
    return response.data;
  } catch (error) {
    console.error(error);
  }
}