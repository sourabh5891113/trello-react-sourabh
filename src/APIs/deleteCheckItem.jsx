import axios from "axios";
import { apiKey, apiToken } from "../secretFile.jsx";

export async function deleteCheckItem(checkListId,checkItemId) {
  try {
    const response = await axios.delete(
      `https://api.trello.com/1/checklists/${checkListId}/checkItems/${checkItemId}?key=${apiKey}&token=${apiToken}`
    );
    return response.data;
  } catch (error) {
    console.error(error);
  }
}
