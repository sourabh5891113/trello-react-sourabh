import {apiKey, apiToken} from "../secretFile.jsx"
import { getBoard } from "./getBoard.jsx";
import axios from 'axios';

export async function getUser(userId) {
  try{
    const response = await axios.get(`https://api.trello.com/1/members/${userId}?fields=fullName,url,username,idBoards&key=${apiKey}&token=${apiToken}`);
    const boardIds = response.data.idBoards;
    const boardsArray = await Promise.all(boardIds.map((id)=>getBoard(id)));
    return boardsArray;
  }catch(e){
    console.error(e);
  }
}