import { apiKey, apiToken } from "../secretFile.jsx";
import axios from 'axios';

export async function getBoard(boardId) {
  try {
    const response = await axios.get(`https://api.trello.com/1/boards/${boardId}?fields=name,url,prefs&key=${apiKey}&token=${apiToken}`);
    return response.data;
  } catch (error) {
    console.error(error);
  }
}
