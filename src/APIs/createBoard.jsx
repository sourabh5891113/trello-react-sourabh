import axios from "axios";
import { apiKey, apiToken } from "../secretFile.jsx";

let colorArr = [
  "blue",
  "orange",
  "green",
  "red",
  "purple",
  "pink",
  "lime",
  "sky",
  "grey",
];

export async function createBoard(boardName) {
  try {
    let ind = Math.floor(Math.random() * 10);
    let color = colorArr[ind];
    const response = await axios.post(
      `https://api.trello.com/1/boards/?prefs_background=${color}&name=${boardName}&key=${apiKey}&token=${apiToken}`
    );
    if (response.status === 200) {
      return response.data;
    }
  } catch (e) {
    console.error(e);
  }
}
