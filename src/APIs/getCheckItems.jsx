import { apiKey, apiToken } from "../secretFile.jsx";
import axios from "axios";

export async function getCheckItems(checkListId) {
  try {
    const response = await axios.get(`https://api.trello.com/1/checklists/${checkListId}/checkItems?key=${apiKey}&token=${apiToken}`);
    return response.data;
  } catch (error) {
    console.error(error);
  }
}