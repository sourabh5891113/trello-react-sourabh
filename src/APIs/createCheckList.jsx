import axios from "axios";
import { apiKey, apiToken } from "../secretFile.jsx";

export async function createCheckList(checkListName,cardId) {
  try {
    const response = await axios.post(
      `https://api.trello.com/1/cards/${cardId}/checklists?name=${checkListName}&key=${apiKey}&token=${apiToken}`
    );
    if (response.status === 200) {
      return response.data;
    }
  } catch (e) {
    console.error(e);
  }
}
