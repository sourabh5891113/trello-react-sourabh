import { apiKey, apiToken } from "../secretFile.jsx";
import axios from "axios";

export async function getCheckLists(cardId) {
  try {
    const response = await axios.get(`https://api.trello.com/1/cards/${cardId}/checklists?key=${apiKey}&token=${apiToken}`);
    return response.data;
  } catch (error) {
    console.error(error);
  }
}