import axios from "axios";
import { apiKey, apiToken } from "../secretFile.jsx";

export async function createCheckItem(checkItemName,checkListId) {
  try {
    const response = await axios.post(
      `https://api.trello.com/1/checklists/${checkListId}/checkItems?name=${checkItemName}&key=${apiKey}&token=${apiToken}`
    );
    if (response.status === 200) {
      return response.data;
    }
  } catch (e) {
    console.error(e);
  }
}
