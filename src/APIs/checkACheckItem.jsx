import axios from "axios";
import { apiKey, apiToken } from "../secretFile.jsx";

export async function checkACheckItem(cardId, checkitemId) {
  try {
    const response = await axios.put(
      `https://api.trello.com/1/cards/${cardId}/checkItem/${checkitemId}?state=complete&key=${apiKey}&token=${apiToken}`
    );
    return response.data;
  } catch (error) {
    console.error(error);
  }
}
