import axios from "axios";
import { apiKey, apiToken } from "../secretFile.jsx";

export async function archiveList(listId) {
  try {
    const response = await axios.put(
      `https://api.trello.com/1/lists/${listId}/closed?value=true&key=${apiKey}&token=${apiToken}`
    );
    return response.data;
  } catch (error) {
    console.error(error);
  }
}