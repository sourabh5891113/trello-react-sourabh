import { useEffect, useState } from "react";
import { Routes, Route } from "react-router-dom";

import AllBoards from "./components/AllBoards";
import BoardDetails from "./components/BoardDetails";
import Header from "./components/Header";
import Sidebar from "./components/Sidebar";

import { getUser } from "./APIs/getUser";
import { createBoard } from "./APIs/createBoard";
import { deleteBoard } from "./APIs/deleteBoard";

import "./App.css";

function App() {
  const [boards, setBoards] = useState([]);
  const [search, setSearch] = useState("");
  async function createNewBoard(boardName, handleNoteColor) {
    const newBoard = await createBoard(boardName);
    if (newBoard) {
      setBoards((prev) => [...prev, newBoard]);
    }
    handleNoteColor(newBoard);
  }
  async function deleteTheBoard(boardId) {
    setBoards((prev) => {
      const newBoards = prev.filter((board) => board.id !== boardId);
      return newBoards;
    });
    await deleteBoard(boardId);
  }
  useEffect(() => {
    let userId = "sourabhthakur11";
    async function getBoards() {
      let data = await getUser(userId);
      setBoards(data);
    }
    getBoards();
  }, []);
  return (
    <>
      <Routes>
        <Route
          path="/"
          element={
            <>
              <Header search={search} setSearch={setSearch} />
              <AllBoards
                boards={boards}
                createNewBoard={createNewBoard}
                isRecent
              />
              <AllBoards
                search={search}
                boards={boards}
                createNewBoard={createNewBoard}
              />
            </>
          }
        />
        <Route
          path="/board/:boardId/:boardName"
          element={
            <>
              <Header isBoard />
              <Sidebar boards={boards} deleteTheBoard={deleteTheBoard} />
              <BoardDetails />
            </>
          }
        />
      </Routes>
    </>
  );
}

export default App;
