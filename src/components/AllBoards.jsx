import React, { useEffect, useState } from "react";
import { Box, Container, Typography } from "@mui/material";

import Board from "./Board";
import CreateNewBoard from "./CreateNewBoard";
import getSessionStorage from "../utils/getSessionStorage";

export default function AllBoards({
  isRecent,
  boards,
  createNewBoard,
  search,
}) {
  const [recentBoards,setRecentBoards] = useState([]);
  useEffect(()=>{
    const newRecentBoards = getSessionStorage();
    setRecentBoards(newRecentBoards);
  },[]);
  return (
    <Container maxWidth="xl">
      <div className="md:px-32 ">
        <Box mb={4} sx={{ display: "flex", alignItems: "center", gap: "1rem" }}>
          {isRecent && (
            <div className="w-4 h-4">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                <path d="M464 256A208 208 0 1 1 48 256a208 208 0 1 1 416 0zM0 256a256 256 0 1 0 512 0A256 256 0 1 0 0 256zM232 120V256c0 8 4 15.5 10.7 20l96 64c11 7.4 25.9 4.4 33.3-6.7s4.4-25.9-6.7-33.3L280 243.2V120c0-13.3-10.7-24-24-24s-24 10.7-24 24z" />
              </svg>
            </div>
          )}
          <Typography variant={!isRecent ? "h5" : "h6"}>
            {!isRecent ? "YOUR BOARDS" : "Recently viewed"}
          </Typography>
        </Box>
        <div className="flex flex-wrap gap-8 justify-center md:justify-normal mb-8">
          {isRecent
            ? recentBoards.map((board) => {
                return (
                  <Board key={board.id} board={board} />
                );
              })
            : boards
                ?.filter((board) => {
                  return board.name.toLowerCase().includes(search.toLowerCase());
                })
                .map((board) => {
                  return (
                    <Board
                      key={board.id}
                      board={board}                    
                    />
                  );
                })}
          {!isRecent && <CreateNewBoard createNewBoard={createNewBoard} />}
        </div>
      </div>
    </Container>
  );
}
