import { Box, Button, TextField } from "@mui/material";
import React, { useState } from "react";

import GrayButton from "./GrayButton";

export default function CretateNewCheckItem({
  isModelOpen,
  setIsModelOpen,
  handleClick,
  checkListId,
  addCheckItem
}) {
  const [input, setInput] = useState("");
  async function handleAddCheckItem(e) {
    e.preventDefault();
    const checkItemName = input;
    setInput("");
    if (checkItemName === "") return;
    addCheckItem(checkItemName,checkListId);
  }
  return (
    <Box
      component="form"
      className={isModelOpen ? "" : "hidden"}
      onSubmit={(e) => handleAddCheckItem(e)}
    >
      <TextField
        id="outlined-multiline-static"
        label="Add an item"
        multiline
        fullWidth
        rows={2}
        value={input}
        onChange={(e) => setInput(e.target.value)}
        required
      />
      <Box sx={{ mt: 1, display: "flex", gap: "1rem" }}>
        <Button
          type="submit"
          variant="contained"
          sx={{ textTransform: "none" }}
        >
          Add
        </Button>
        <GrayButton setIsModelOpen={setIsModelOpen} onClick={handleClick}>
          Cancel
        </GrayButton>
      </Box>
    </Box>
  );
}
