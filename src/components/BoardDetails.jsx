import { Container, Typography } from "@mui/material";
import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";

import AllLists from "./AllLists";

import { getLists } from "../APIs/getLists";
import { createList } from "../APIs/createList";
import { archiveList } from "../APIs/archiveList";

export default function BoardDetails() {
  const navigate = useNavigate();
  const [lists, setLists] = useState([]);
  const { boardId,boardName } = useParams();
  async function createListOrCard(listName) {
      let data = await createList(listName, boardId);
      setLists((prev) => {
        let newLists = [...prev];
        newLists.push(data);
        return newLists;
      });
  }
  async function deleteList(listId) {
    if (listId !== undefined) {
      setLists((prev) => {
        const newLists = prev.filter((list) => list.id !== listId);
        return newLists;
      });
      await archiveList(listId);
    }
  }
  useEffect(() => {
    try {
      async function getAllLists() {
        const data = await getLists(boardId);
        if (data === undefined) {
          let rv = JSON.parse(sessionStorage.getItem("rv"));
          let newRv = rv.filter((board) => board.id !== boardId);
          sessionStorage.setItem("rv", JSON.stringify(newRv));
          navigate("/");
        }
        setLists(data);
      }
      getAllLists();
    } catch (e) {
      console.error(e);
    }
  }, [boardId]);
  return (
    <Container
      sx={{ mr: 0, ml: "19rem" }}
      style={{ maxWidth: "-webkit-fill-available" }}
    >
      <Typography variant="h4" sx={{ my: 2 }}>
        {boardName}
      </Typography>
      <AllLists
        lists={lists}
        createListOrCard={createListOrCard}
        deleteList={deleteList}
      />
    </Container>
  );
}
