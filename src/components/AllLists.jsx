import React from 'react';

import ListComp from './ListComp';
import CreateNewListOrCard from './createNewListOrCard';

export default function AllLists({lists,createListOrCard, deleteList}) {
  return (
    <div className="p-3 flex flex-wrap gap-4">
      {lists.map((list)=>  <ListComp key={list.id} list={list}  deleteList={deleteList}/>)}
      <CreateNewListOrCard createListOrCard={createListOrCard}/>
    </div>
  )
}
