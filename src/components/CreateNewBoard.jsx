import { Box } from "@mui/material";
import React, { useRef, useState } from "react";
import SimpleSnackbar from "./SimpleSnackbar";

export default function CreateNewBoard({createNewBoard}) {
  const [isModelOpen, setIsModelOpen] = useState(false);
  const [open, setOpen] = useState(false);
  const [noteColor,setNoteColor] = useState(true);
  const inputRef = useRef(null);
  const style = { backgroundColor: "rgba(9, 30, 66, 0.14)" };
  let clas;
  if (isModelOpen) {
    clas = "flex flex-col justify-center items-center  p-1 gap-2";
  } else {
    clas = "hidden flex-col justify-center items-center  p-1 gap-2";
  }

  function handleCreateBoard(e){
    e.preventDefault();
    const boardName = inputRef.current.value;
    inputRef.current.value = "";
    createNewBoard(boardName,handleNoteColor);
    setIsModelOpen(false);
  }
  function handleNoteColor(newBoard){
    if (newBoard) {
      setNoteColor(true);
    } else {
      setNoteColor(false);
    }
    setOpen(true);
  }
  return (
    <div className="flex relative gap-4">
      <div
        className="card w-52 h-32 rounded flex justify-center items-center cursor-pointer"
        style={style}
        id="create-new-board"
        onClick={() => setIsModelOpen(!isModelOpen)}
      >
        <h4>Create new Board</h4>
      </div>
      <Box
        component="form"
        className={clas}
        style={style}
        id="create-new-board-model"
        onSubmit={(e)=>handleCreateBoard(e)}
      >
        <h4>Create board</h4>
        <span
          className="absolute top-0 right-6 text-2xl cursor-pointer"
          onClick={() => setIsModelOpen(false)}
        >
          x
        </span>
        <label htmlFor="Board title" className="text-sm self-start">
          Board title
        </label>
        <input
          type="text"
          required
          className="border border-black px-1 rounded"
          ref={inputRef}
        />
        <input
          type="submit"
          className="text-white bg-blue-700 hover:bg-blue-800 font-medium rounded text-sm px-5 py-1  focus:outline-none w-full"
          id="create-button"
          value="Create"
        />
      </Box>
      <SimpleSnackbar open={open} setOpen={setOpen} noteColor={noteColor}/>
    </div>
  );
}
