import {
  Divider,
  Drawer,
  List,
  Toolbar,
} from "@mui/material";
import React from "react";

import BoardInList from "./BoardInList";

const drawerWidth = 300;
export default function Sidebar({ boards, deleteTheBoard }) {
  const drawer = (
    <div>
      <Toolbar>
        <div className="flex items-center gap-4 flex-row w-full">
          <div className="t-div w-[15%] h-10 ml-2 flex items-center justify-center text-white">
            T
          </div>
          <div className="flex flex-col">
            <h2>Trello Workspace</h2>
            <p>Free</p>
          </div>
        </div>
      </Toolbar>
      <Divider />
      <List>
        {boards.map((board) => (
          <BoardInList key={board.id} board={board} deleteTheBoard={deleteTheBoard} />
        ))}
      </List>
      <Divider />
    </div>
  );
  return (
    <Drawer
      variant="permanent"
      sx={{
        display: { xs: "none", sm: "block" },
        "& .MuiDrawer-paper": { boxSizing: "border-box", width: drawerWidth },
      }}
      open
      className="bg-black"
    >
      {drawer}
    </Drawer>
  );
}
