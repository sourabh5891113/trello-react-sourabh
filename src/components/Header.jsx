import React, { useRef } from "react";
import { Button, TextField } from "@mui/material";
import { useNavigate } from "react-router-dom";

import logo from "/logo.png";

export default function Header({ isBoard,setSearch }) {
  const navigate = useNavigate();
  const inputRef = useRef(null);
  function handleSearchChange(e){
    e.preventDefault();
    setSearch(inputRef.current.value);
  }
  const style = isBoard? { marginLeft: "18rem", maxWidth: "-webkit-fill-available", boxShadow: "0 -6px 10px 5px rgba(0,0,0,0.5)" }: {};
  return (
    <header className="flex justify-between min-h-16 md:px-4 md:py-8 px-2 py-4 max-w-screen-2xl mx-auto" style={style}>
      <div>
        <img src={logo} alt="Trello logo" className="w-40" />
      </div>
      {isBoard ? (
        <Button variant="outlined" onClick={()=> navigate("/")}>Boards</Button>
      ) : (
        <form className=" hidden md:flex items-end gap-2" onSubmit={(e)=>handleSearchChange(e)}>
          <TextField
            id="standard-basic"
            label="Enter Board Name"
            variant="standard"
            inputRef={inputRef}
          />
          <Button variant="contained" >Search Boards</Button>
        </form>
      )}
    </header>
  );
}
