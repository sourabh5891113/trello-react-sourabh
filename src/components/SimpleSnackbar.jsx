import * as React from "react";
import Snackbar from "@mui/material/Snackbar";
import { Alert } from "@mui/material";

export default function SimpleSnackbar({ open, setOpen,noteColor }) {
  const handleClick = () => {
    setOpen(true);
  };

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpen(false);
  };


  return (
    <div>
      <Snackbar open={open} autoHideDuration={3000} onClose={handleClose}>
        <Alert
          onClose={handleClose}
          severity={noteColor ?"success" : "error"}
          variant="filled"
          sx={{ width: "100%" }}
        >
         {noteColor ? "New Board Created" : "Error in creating Board"}
        </Alert>
      </Snackbar>
    </div>
  );
}
