import { IconButton, Menu, MenuItem } from "@mui/material";
import MoreVertIcon from "@mui/icons-material/MoreVert";
import React from "react";
import { useNavigate } from "react-router-dom";

const ITEM_HEIGHT = 48;
const options = ["Delete Board"];
export default function DeleteBoard({
  anchorEl,
  setAnchorEl,
  board,
  deleteTheBoard,
}) {
  const navigate = useNavigate();
  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    event.stopPropagation();
    setAnchorEl(event.currentTarget);
  };
  const handleDelete = async (e,boardId) => {
    e.stopPropagation();
    setAnchorEl(null);
    if (e.target.tagName !== "LI") return;
    let rv = JSON.parse(sessionStorage.getItem("rv"));
    let newRv = rv.filter((board) => board.id !== boardId);
    sessionStorage.setItem("rv", JSON.stringify(newRv));
    navigate("/");
    deleteTheBoard(boardId);
  };
  return (
    <div>
      <IconButton
        aria-label="more"
        id="long-button"
        aria-controls={open ? "long-menu" : undefined}
        aria-expanded={open ? "true" : undefined}
        aria-haspopup="true"
        onClick={handleClick}
      >
        <MoreVertIcon />
      </IconButton>
      <Menu
        id="long-menu"
        MenuListProps={{
          "aria-labelledby": "long-button",
        }}
        anchorEl={anchorEl}
        open={open}
        onClose={() => setAnchorEl(null)}
        sx={{
          style: {
            maxHeight: ITEM_HEIGHT * 4.5,
            width: "20ch",
          },
        }}
      >
        {options.map((option) => (
          <MenuItem
            key={option}
            selected={option === "Pyxis"}
            onClick={(e) => handleDelete(e,board.id)}
          >
            {option}
          </MenuItem>
        ))}
      </Menu>
    </div>
  );
}
