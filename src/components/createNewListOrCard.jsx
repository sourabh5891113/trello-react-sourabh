import { Box, Button, TextField } from "@mui/material";
import React, { useRef, useState } from "react";

import CloseButton from "./CloseButton";

export default function CreateNewListOrCard({
  createListOrCard,
  isCard,
}) {
  const [isModelOpen, setIsModelOpen] = useState(false);
  const inputRef = useRef(null);
  async function handleAdd(e) {
    e.preventDefault();
    const elementName = inputRef.current.value;
    inputRef.current.value = "";
    if (elementName === "") return;
    createListOrCard(elementName);
  }
  return (
    <div className={isCard ? "" : "w-[350px]"}>
      <div
        className={
          isCard
            ? `flex min-h-10 mt-2 gap-2 items-center ${
                isModelOpen ? "" : "hover:bg-white"
              }  pl-3 rounded-lg add-card`
            : "flex min-h-10 mt-2 gap-2 items-center bg-blue-300 p-4 rounded-lg"
        }
      >
        <div
          className={isModelOpen ? "hidden gap-3" : "flex gap-3 w-full"}
          onClick={() => setIsModelOpen(true)}
        >
          <p>+</p>
          <p>Add {isCard ? "a card" : "Another List"}</p>
        </div>

        <Box
          component="form"
          className={
            isModelOpen ? "flex flex-wrap gap-3" : "hidden flex-wrap gap-3"
          }
          onSubmit={handleAdd}
        >
          <TextField
            id="standard-basic"
            label={
              isCard ? "Enter a title for this card..." : "Enter List Tilte..."
            }
            variant="standard"
            fullWidth
            inputRef={inputRef}
          />
          <div className="flex gap-3">
            <Button type="submit" variant="contained">
              Add {isCard ? "card" : " List"}
            </Button>
            <button
              className="btn btn-square"
              id="close-btn-card"
              onClick={(e) => {
                e.preventDefault();
                setIsModelOpen(false);
              }}
            >
              <CloseButton />
            </button>
          </div>
        </Box>
      </div>
    </div>
  );
}
