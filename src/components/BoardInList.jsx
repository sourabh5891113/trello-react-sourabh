import { ListItem, ListItemButton, ListItemText } from "@mui/material";
import React from "react";
import { useNavigate, useParams } from "react-router-dom";

import DeleteBoard from "./DeleteBoard";

export default function BoardInList({ board,deleteTheBoard }) {
    const navigate = useNavigate();
  const { boardId } = useParams();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const handleChange = (board) => {
    if (board.id !== boardId) {
      navigate(`/board/${board.id}/${board.name}`);
    }
  };
  return (
    <ListItem key={board.id} disablePadding>
      <ListItemButton onClick={() => handleChange(board)} sx={{ gap: "1rem" }}>
        <div
          className="w-5 h-4 rounded-sm bg-blue-500 pointer-events-none"
          style={{ backgroundColor: board.prefs.backgroundColor }}
        ></div>
        <ListItemText primary={board.name} />
        <DeleteBoard
          anchorEl={anchorEl}
          setAnchorEl={setAnchorEl}
          board={board}
          deleteTheBoard={deleteTheBoard}
        />
      </ListItemButton>
    </ListItem>
  );
}
