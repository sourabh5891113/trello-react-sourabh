import React from "react";
import { useNavigate } from "react-router-dom";
import setSessionStorage from "../utils/setSessionStorage";

export default function Board({ board }) {
    const navigate = useNavigate();
    const { id,name,prefs } = board;
    function handleClick(){
        setSessionStorage(id,board);
        navigate(`/board/${id}/${name}`);
    }
    const {backgroundImage, backgroundColor } = prefs || {};
    let style = backgroundImage ? { backgroundImage: `url(${backgroundImage})`, backgroundSize: "cover"} : {backgroundColor};
  return (
    <div
      className="card w-52 h-32 rounded text-white font-bold p-2 min-w-[192px]"
      style={style}
      onClick={handleClick}
    >
        <h4>{name}</h4>
    </div>
  );
}
