import { Box, Button, Popover, TextField, Typography } from "@mui/material";
import React, { useRef, useState } from "react";

import GrayButton from "./GrayButton";
import CloseButton from "./CloseButton";
import checkboxImg from "/checkbox.svg";

export default function CreateNewCheckList({ createNewCheckList }) {
  const [anchorEl, setAnchorEl] = useState(null);
  const inputRef = useRef(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleAddCheckList = async (e) => {
    e.preventDefault();
    const checkListName = inputRef.current.value;
    inputRef.current.value = "";
    if (checkListName === "") return;
    createNewCheckList(checkListName);
    setAnchorEl(null);
  };
  const open = Boolean(anchorEl);
  const id = open ? "simple-popover" : undefined;
  return (
    <Box>
      <GrayButton isChk isFull onClick={handleClick}>
        <img src={checkboxImg} alt="checkbox" className="w-5" />
        <Typography id="modal-modal-title" variant="h8" component="h5">
          Checklist
        </Typography>
      </GrayButton>
      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "left",
        }}
        sx={{ textAlign: "center", mt: 1 }}
      >
        <Box
          component="form"
          sx={{ width: "17rem", p: 2 }}
          onSubmit={(e) => handleAddCheckList(e)}
        >
          <Typography
            variant="p"
            sx={{ fontSize: "14px", fontWeight: 700, mx: "auto" }}
          >
            Add checklist
          </Typography>
          <CloseButton isSmall onClick={handleClose} />
          <TextField
            id="standard-basic"
            label="Title"
            variant="standard"
            inputRef={inputRef}
          />
          <Button type="submit" variant="contained" sx={{ mt: 1 }}>
            Add
          </Button>
        </Box>
      </Popover>
    </Box>
  );
}
