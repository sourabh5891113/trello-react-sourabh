import { Button } from "@mui/material";
import React from "react";

export default function GrayButton({ isModelOpen, children,Del, isFull, onClick}) {
  
  return (
    <Button
      variant="contained"
      sx={{
        bgcolor: "rgba(9, 30, 66, 0.06)",
        color: "rgb(23, 43, 77)",
        ":hover": { bgcolor: "rgba(9, 30, 66, 0.2)" },
        textTransform: "none",
        display: `${isModelOpen ? "none" : ""}`,
        gap: "0.5rem",
        position: `${Del ? "absolute" : ""}`,
        right: `${Del ? "20px" : ""}`,
        width: `${isFull? "100%": "initial"}`
      }}
      onClick={onClick}
    >
      {children}
    </Button>
  );
}
