import { Box, Typography } from "@mui/material";
import React, { useState } from "react";

import CheckItem from "./CheckItem";
import CretateNewCheckItem from "./CretateNewCheckItem";
import GrayButton from "./GrayButton";
import LinearProgressWithLabel from "./LinearProgressWithLabel";
import checkboxImg from "/checkbox.svg";
import calculateProgress from "../utils/calculateProgress";

export default function CheckList({
  checklist,
  handleCheckListDelete,
  addCheckItem,
  handleCheckItemDelete,
  handleCheckItemChange,
}) {
  const checkItems = checklist.checkItems;
  const checkListId = checklist.id;
  const [isModelOpen, setIsModelOpen] = useState(false);
  const progress = calculateProgress(checkItems);

  function handleClick() {
    setIsModelOpen((prev) => !prev);
  }
  return (
    <Box sx={{ mb: 5 }}>
      <Box component="div" sx={{ position: "relative", mb: 2 }}>
        <Box
          component="div"
          sx={{ display: "flex", gap: "1rem", alignItems: "center" }}
        >
          <img src={checkboxImg} alt="checkbox" className="w-7" />
          <Typography id="modal-modal-title" variant="h6" component="h2">
            {checklist?.name}
          </Typography>
          <GrayButton
            isChk
            Del
            onClick={() => handleCheckListDelete(checkListId)}
          >
            Delete
          </GrayButton>
        </Box>
      </Box>
      {/* progess bar */}
      <Box sx={{ width: "100%" }}>
        <LinearProgressWithLabel value={progress} />
      </Box>
      {/* checkitems */}
      {checkItems.map((checkItem) => (
        <CheckItem
          key={checkItem.id}
          checkItem={checkItem}
          checkListId={checkListId}
          handleCheckItemDelete={handleCheckItemDelete}
          handleCheckItemChange={handleCheckItemChange}
        />
      ))}

      {/* add checkitem */}
      <Box sx={{ mt: 1 }}>
        <GrayButton
          setIsModelOpen={setIsModelOpen}
          isModelOpen={isModelOpen}
          onClick={handleClick}
        >
          Add an item
        </GrayButton>

        {/* form for checkitem*/}
        <CretateNewCheckItem
          isModelOpen={isModelOpen}
          setIsModelOpen={setIsModelOpen}
          handleClick={handleClick}
          checkListId={checkListId}
          addCheckItem={addCheckItem}
        />
      </Box>
    </Box>
  );
}
