import React, { useEffect, useState } from "react";
import List from "@mui/material/List";
import { Box, IconButton, Menu, MenuItem, TextField } from "@mui/material";
import MoreVertIcon from "@mui/icons-material/MoreVert";

import CardComp from "./CardComp";
import CreateNewListOrCard from "./createNewListOrCard";

import { getCards } from "../APIs/getCards";
import { createCard } from "../APIs/createCard";
import { deleteCard } from "../APIs/deleteCard";

const ITEM_HEIGHT = 48;
const options = ["Delete List"];

export default function ListComp({ list, deleteList }) {
  const [cards, setCards] = useState([]);
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    event.stopPropagation();
    setAnchorEl(event.currentTarget);
  };
  const handleDelete = async (e,listId) => {
    e.stopPropagation();
    setAnchorEl(null);
    if (e.target.tagName !== "LI") return;
    deleteList(listId);
  };
  async function  createListOrCard(CardName){
    let data = await createCard(CardName, list.id);
      setCards((prev) => {
        let newCards = [...prev];
        newCards.push(data);
        return newCards;
      });
  }
  async function deleteTheCard(cardId){
    setCards((prev) => {
      const newCards = prev.filter((card) => card.id !== cardId);
      return newCards;
    });
    await deleteCard(cardId);
  }
  useEffect(() => {
    async function getAllCards() {
      const data = await getCards(list.id);
      setCards(data);
    }
    getAllCards();
  }, []);
  return (
    <Box
      sx={{
        flexGrow: 1,
        maxWidth: 352,
        bgcolor: "#ebecf0",
        p: 2,
        borderRadius: 3,
        height: "fit-content",
      }}
    >
      <List>
        <div className="flex items-center">
          <TextField
            id="outlined-basic"
            label=""
            variant="outlined"
            value={list?.name}
            sx={{
              "& .MuiOutlinedInput-root": {
                width: "80%",
                color: "#000",
                fontWeight: "bold",
                fontSize: "25px",
                py: "0",
                // Class for the border around the input field
                "& .MuiOutlinedInput-notchedOutline": {
                  borderWidth: "0px",
                },
              },
            }}
          />

          <div>
            <IconButton
              aria-label="more"
              id="long-button"
              aria-controls={open ? "long-menu" : undefined}
              aria-expanded={open ? "true" : undefined}
              aria-haspopup="true"
              onClick={handleClick}
            >
              <MoreVertIcon />
            </IconButton>
            <Menu
              id="long-menu"
              MenuListProps={{
                "aria-labelledby": "long-button",
              }}
              anchorEl={anchorEl}
              open={open}
              onClose={() => setAnchorEl(null)}
              sx={{
                style: {
                  maxHeight: ITEM_HEIGHT * 4.5,
                  width: "20ch",
                },
              }}
            >
              {options.map((option) => (
                <MenuItem
                  key={option}
                  selected={option === "Pyxis"}
                  onClick={(e) => handleDelete(e,list.id)}
                >
                  {option}
                </MenuItem>
              ))}
            </Menu>
          </div>
        </div>
        {cards.map((card) => (
          <CardComp key={card.id} card={card} deleteTheCard={deleteTheCard} list={list} />
        ))}
        <CreateNewListOrCard
          createListOrCard={createListOrCard}
          isCard
        />
      </List>
    </Box>
  );
}
