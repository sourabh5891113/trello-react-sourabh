import React, { useEffect, useState } from "react";
import {
  Box,
  IconButton,
  ListItem,
  ListItemText,
  Modal,
  Typography,
} from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";

import CheckList from "./CheckList";
import CreateNewCheckList from "./CreateNewCheckList";
import CloseButton from "./CloseButton";
import checkboxImg from "/checkbox.svg";
import calculateNewCheckItems from "../utils/calculateNewCheckItems";

import { getCheckLists } from "../APIs/getCheckLists";
import { createCheckList } from "../APIs/createCheckList";
import { deleteCheckList } from "../APIs/deleteCheckList";
import { createCheckItem } from "../APIs/createCheckItem";
import { deleteCheckItem } from "../APIs/deleteCheckItem";
import { checkACheckItem } from "../APIs/checkACheckItem";
import { uncheckACheckItem } from "../APIs/uncheckACheckItem";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 750,
  bgcolor: "background.paper",
  color: "#172b4d",
  boxShadow: 24,
  p: 4,
  display: "flex",
};

export default function CardComp({ card, deleteTheCard, list }) {
  const [open, setOpen] = useState(false);
  const [checkLists, setCheckLists] = useState([]);
  const checkitems = checkLists.reduce((acc, chk) => {
    acc = [...acc, ...chk.checkItems];
    return acc;
  }, []);
  const checkedCIs = checkitems.reduce((acc, ci) => {
    if (ci.state === "complete") acc++;
    return acc;
  }, 0);
  let newData = checkitems.length === 0 ? null : checkedCIs + "/" + checkitems.length;
  const handleClose = () => setOpen(false);
  async function createNewCheckList(checkListName){
    const data = await createCheckList(checkListName,card.id);
    setCheckLists((prev)=>{
        let newCheckLists = [...prev];
        newCheckLists.push(data);
        return newCheckLists;
    });
  }
  async function handleDeleteCard(e, cardId) {
    e.stopPropagation();
    deleteTheCard(cardId)
  }
  async function handleCheckListDelete(checkListId) {
    setCheckLists((prev) => {
      let newCheckLists = prev.filter(
        (checkList) => checkList.id !== checkListId
      );
      return newCheckLists;
    });
    await deleteCheckList(checkListId);
  }
  async function addCheckItem(checkItemName,checkListId){
    const data = await createCheckItem(checkItemName, checkListId);;
    setCheckLists((prev) => {
      const newCheckLists = prev.map((checkList) => {
        if (checkList.id === checkListId) {
          const newCheckItems = [...checkList.checkItems];
          newCheckItems.push(data);
          return { ...checkList, checkItems: newCheckItems };
        } else {
          return checkList;
        }
      });
      return newCheckLists;
    });
  }
  async function handleCheckItemDelete(checkItemId,checkListId) {
    setCheckLists((prev) => {
      const newCheckLists = prev.map((checkList) => {
        if (checkList.id === checkListId) {
          const newCheckItems = checkList.checkItems.filter(
            (checkItem) => checkItem.id !== checkItemId
          );
          return { ...checkList, checkItems: newCheckItems };
        } else {
          return checkList;
        }
      });
      return newCheckLists;
    });
    await deleteCheckItem(checkListId, checkItemId);
  }
  async function handleCheckItemChange(checkItem,checkListId) {
    setCheckLists((prev) => {
      const newCheckLists = prev.map((checkList) => {
        if (checkList.id === checkListId) {
          const newCheckItems = calculateNewCheckItems(
            checkList.checkItems,
            checkItem.id
          );
          return { ...checkList, checkItems: newCheckItems };
        } else {
          return checkList;
        }
      });
      return newCheckLists;
    });
    if (checkItem.state === "complete") {
      await uncheckACheckItem(card.id, checkItem.id);
    } else {
      await checkACheckItem(card.id, checkItem.id);
    }
  }
  useEffect(() => {
    async function getAllCheckLists() {
      const data = await getCheckLists(card.id);
      setCheckLists(data);
    }
    getAllCheckLists();
  }, []);
  return (
    <>
      <ListItem
        sx={{
          bgcolor: "white",
          my: 2,
          borderRadius: 3,
          flexDirection: "column",
          alignItems: "flex-start",
        }}
        secondaryAction={
          <IconButton
            edge="end"
            aria-label="delete"
            onClick={(e) => handleDeleteCard(e, card.id)}
          >
            <DeleteIcon />
          </IconButton>
        }
        onClick={()=>setOpen(true)}
      >
        <ListItemText primary={card.name} secondary={null} />
        <Box sx={{ display: "flex", gap: "10px" }}>
          {newData && <img src={checkboxImg} alt="checkbox" className="w-4" />}
          <ListItemText primary={null} secondary={newData} />
        </Box>
      </ListItem>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Box component="div" sx={{ minWidth: "450px", width: "75%" }}>
            <Box component="div" sx={{ display: "flex", gap: "1rem" }}>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 576 512"
                className="w-5"
              >
                <path
                  fill="#172b4d"
                  d="M512 80c8.8 0 16 7.2 16 16v32H48V96c0-8.8 7.2-16 16-16H512zm16 144V416c0 8.8-7.2 16-16 16H64c-8.8 0-16-7.2-16-16V224H528zM64 32C28.7 32 0 60.7 0 96V416c0 35.3 28.7 64 64 64H512c35.3 0 64-28.7 64-64V96c0-35.3-28.7-64-64-64H64zm56 304c-13.3 0-24 10.7-24 24s10.7 24 24 24h48c13.3 0 24-10.7 24-24s-10.7-24-24-24H120zm128 0c-13.3 0-24 10.7-24 24s10.7 24 24 24H360c13.3 0 24-10.7 24-24s-10.7-24-24-24H248z"
                />
              </svg>
              <Typography
                id="modal-modal-title"
                variant="h5"
                component="h2"
                sx={{ fontWeight: "700" }}
              >
                {card.name}
              </Typography>
            </Box>
            <Box component="div" sx={{ marginBottom: "2rem" }}>
              <Typography variant="h7" sx={{ fontWeight: "300", ml: 4.5 }}>
                in list <span className="underline">{list.name}</span>
              </Typography>
            </Box>
            {/* checklist */}
            {checkLists.map((checklist) => (
              <CheckList
                key={checklist.id}
                checklist={checklist}
                handleCheckListDelete={handleCheckListDelete}
                addCheckItem={addCheckItem}
                handleCheckItemDelete={handleCheckItemDelete}
                handleCheckItemChange={handleCheckItemChange}
              />
            ))}
          </Box>

          <Box
            component="div"
            sx={{
              flexGrow: 1,
              pl: 1,
              display: "flex",
              flexDirection: "column",
            }}
          >
            <button className="self-end mb-4 " onClick={handleClose}>
              <CloseButton />
            </button>
            <Typography variant="p" sx={{ fontSize: "12px", fontWeight: 700 }}>
              Add to card
            </Typography>
            <CreateNewCheckList
              createNewCheckList={createNewCheckList}
            />
          </Box>
        </Box>
      </Modal>
    </>
  );
}
