import { Box, Checkbox, IconButton, Typography } from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
import React from "react";

export default function CheckItem({
  checkItem,
  checkListId,
  handleCheckItemDelete,
  handleCheckItemChange
}) {
  return (
    <Box sx={{ display: "flex", alignItems: "center", position: "relative" }}>
      <Checkbox
        sx={{ pl: 0 }}
        checked={checkItem.state === "complete" ? true : false}
        onChange={(e) => handleCheckItemChange(checkItem,checkListId)}
      />
      <Typography
        variant="h7"
        sx={{
          fontWeight: "400",
          textDecoration: `${checkItem.state === "complete" ? "line-through" : ""}`,
        }}
      >
        {checkItem.name}
      </Typography>
      <IconButton
        edge="end"
        aria-label="delete"
        sx={{ position: "absolute", right: "30px" }}
        onClick={() => handleCheckItemDelete(checkItem.id,checkListId)}
      >
        <DeleteIcon sx={{ width: "20px" }} />
      </IconButton>
    </Box>
  );
}
