import React from "react";

export default function CloseButton({isSmall,onClick}) {
  const extraClass = "absolute right-2 top-2";
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      className={isSmall ? `${extraClass} h-5 w-5` : "h-6 w-6"}
      fill="none"
      viewBox="0 0 24 24"
      stroke="currentColor"
      onClick={onClick}
    >
      <path
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="2"
        d="M6 18L18 6M6 6l12 12"
      />
    </svg>
  );
}
